document.addEventListener("DOMContentLoaded", function () {

    var connexion = new MovieDb();

    if (document.location.pathname.search("fiche-film.html") > 0) {
        var params = (new URL(document.location)).searchParams;
        console.log(params.get("id"));

        connexion.requeteInfoFilm(params.get("id"));
        connexion.requeteInfosActeurs(params.get("id"));

    } else {
        connexion.requeteNowPlaying();
        connexion.requetePopular();
    }

    retour.addEventListener("click", function () {
        scrollToTop(50, 16.6);
    });
});

// scroll vers le haut avec fleche footer
var intervalId = 0;

function scrollStep(scrollStepHeight) {
    if (window.pageYOffset === 0) {
        clearInterval(intervalId);
    }
    window.scrollTo(0, window.pageYOffset - scrollStepHeight);
}

function scrollToTop(scrollStepHeight, delay) {
    if (scrollStepHeight <= 0) {
        alert("The specified scroll step height must be positive!");
    } else if (delay <= 0) {
        alert("The specified scroll delay must be positive!");
    }
    intervalId = setInterval(function () {
        scrollStep(scrollStepHeight);
    }, delay);

}

// MovieDb
class MovieDb {
    constructor() {
        this.APIkey = "4d5682b76064d23e0201604604f9390a";
        this.lang = "fr-CA";
        this.baseURL = "https://api.themoviedb.org/3/";
        this.imgPath = "http://image.tmdb.org/t/p/";
        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];
        this.largeurTeteAffiche = ["45", "185"];
        this.filmCaroussel = 9;
        this.totalFilm = 8;
        this.totalActeur = 6;
        this.totalPopulaire = 6;
    }

    // Requete pour les films à l'affiche présentement
    requeteNowPlaying() {
        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;
        xhr.addEventListener("readystatechange", this.retourRequeteNowPlaying.bind(this));
        xhr.open("GET", this.baseURL + "movie/now_playing?page=1&language=" + this.lang + "&api_key=" + this.APIkey);
        xhr.send();
    }

    retourRequeteNowPlaying(e) {
        let target = e.currentTarget;
        let data;
        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText).results;
            this.afficheNowPlaying(data);
        }
    }

    afficheNowPlaying(data) {
        for (var i = 0; i < this.filmCaroussel; i++) {
            let unArticle = document.querySelector(".template>div.swiper-slide").cloneNode(true);
            unArticle.querySelector("h2").innerText = data[i].title;
            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);
            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[5] + data[i].poster_path);
            unArticle.querySelector("#note").innerText = data[i].vote_average;

            document.querySelector(".film-now-playing").appendChild(unArticle);
        }

        var mySwiper = new Swiper('.swiper-container', {
            direction: 'horizontal',
            loop: true,

            // Navigation avec les flèches
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // Barre de navigation
            scrollbar: {
                el: '.swiper-scrollbar',
            }
        })
    }

    // Infos pour un seul film
    requeteInfoFilm(movieId) {
        var xhr = new XMLHttpRequest();
        xhr.addEventListener("readystatechange", this.retourRequeteInfoFilm.bind(this));
        xhr.open("GET", this.baseURL + "movie/" + movieId + "?language=" + this.lang + "&api_key=" + this.APIkey);
        xhr.send();
    }

    retourRequeteInfoFilm(e) {
        var target = e.currentTarget;
        var data;
        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText);
            this.afficheInfoFilm(data);
        }
    }

    afficheInfoFilm(data) {
        document.querySelector("h3").innerText = data.title;
        var uneImage = document.querySelector("img.affiche");
        uneImage.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[5] + data.poster_path);
        document.querySelector("#anneeFiche").innerText = data.release_date;
        document.querySelector("#coteFiche").innerText = data.vote_average;

        if (data.overview == "") {
            document.querySelector("#synopsisFiche").innerText = "Description à venir.";
        } else {
            document.querySelector("#synopsisFiche").innerText = data.overview;
        }
    }

    // Requete pour acteurs
    requeteInfosActeurs(idFilm) {
        var xhr = new XMLHttpRequest();
        xhr.addEventListener("readystatechange", this.retourRequeteInfosActeurs.bind(this));
        xhr.open("GET", this.baseURL + "movie/" + idFilm + "/credits?api_key=" + this.APIkey);
        xhr.send();
    }

    retourRequeteInfosActeurs(e) {
        let target = e.currentTarget;
        let data;
        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText).cast;
            this.afficheInfosActeurs(data);
        }
    }

    afficheInfosActeurs(data) {
        for (var i = 0; i < this.totalActeur; i++) {
            let unActeur = document.querySelector(".template>article.acteur").cloneNode(true);
            unActeur.querySelector("h4").innerText = data[i].name;
            unActeur.querySelector("p").innerText = data[i].character;
            unActeur.querySelector("img").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].profile_path);
            document.querySelector(".liste-acteurs").appendChild(unActeur);
        }

        var mySwiper = new Swiper('.swiper-container', {
            direction: 'horizontal',
            loop: true,

            // Navigation avec les flèches
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // Barre de navigation
            scrollbar: {
                el: '.swiper-scrollbar',
            }
        })
    }

    // Requete pour les films les plus populaires
    requetePopular() {
        var xhr = new XMLHttpRequest();
        xhr.addEventListener("readystatechange", this.retourRequetePopular.bind(this));
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIkey);
        xhr.send();
    }

    retourRequetePopular(e) {
        let target = e.currentTarget;
        let data;
        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText).results;
            this.affichePopular(data);
        }
    }

    affichePopular(data) {
        for (var i = 0; i < this.totalPopulaire; i++) {
            let unArticle = document.querySelector(".template>article.filmPopulaire").cloneNode(true);
            unArticle.querySelector("h3").innerText = data[i].title;
            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);
            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[5] + data[i].poster_path);
            unArticle.querySelector("#note-film-populaire").innerText = data[i].vote_average;

            if (data[i].overview == "") {
                unArticle.querySelector(".synopsis").innerText = "Description à venir.";
            } else {
                unArticle.querySelector(".synopsis").innerText = data[i].overview;
            }

            unArticle.querySelector("#annee").innerText = data[i].release_date;

            document.querySelector(".films-container").appendChild(unArticle);
        }


        var mySwiper = new Swiper('.swiper-container', {
            slidesPerView: 2,
            spaceBetween: 20,
            direction: 'horizontal',
            loop: true,

            // Navigation avec les flèches
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // Barre de navigation
            scrollbar: {
                el: '.swiper-scrollbar',
            },

            breakpoints: {
                768: {
                    slidesPerView: 3
                },
                1440: {
                    slidesPerView: 4
                }
            }
        });

    }

}
